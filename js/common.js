$.validator.addMethod('uaphone', function(value, element, params){
  var regex = /\+38 \(0[0-9]{2}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/g;
  var match = value.match(regex);
  if (match!==null) {
    if (match.length === 1) return true;
  }
  return false;
});

//$.validator.addMethod('area', function(value, element, params){
//  if (value > 250 || value < 10) {
//    return false;
//  };
//  return true;
//});

$(document).ready(function() {
  $('input[name="phone"]').mask('+38 (999) 999-99-99');
  //$('input[name="area"]').on('keydown', function(e){
  //  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
  //      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
  //      (e.keyCode >= 35 && e.keyCode <= 40)) {
  //    return;
  //  }
  //  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
  //    e.preventDefault();
  //  }
  //});
  var forms = document.getElementsByClassName('formGo');
  for (var i = 0; i < forms.length; i++) {
    $(forms[i]).validate({
      rules: {
        phone: {
          required: true,
          uaphone: true,
        },
      },
      messages: {
        phone: {
          required: 'Введите номер телефона',
          uaphone: 'Введите корректный номер телефона'
        }
      },
      submitHandler: submitForm
    });
  };
  $('[data-init="modal"]').on('click', function() {
    var selector = $(this).attr('data-modal');		//переменной присваивается значение атрибута (это пока еще одна кнопка)
    var button_name = $(this).attr('data-google');			//переменной присваивается значение атрибута (это пока еще одна кнопка)
    $(selector).addClass('active');					//блоку с этим трибутом добавляется класс
    var toggle = $(this).attr('data-input');		//переменно присваивается значение атрибута
    $('#order').val(toggle);						//айДишнику присваивается значение переменной
    $(selector).find("input[name='htmlData']").val(button_name);			//хиден инпуту присваиваю значение кнопки
  });
  $('.modal-close').on('click', function() {
    $(this).parent().parent().removeClass('active');
  });
  $('[data-type="modal"]').on('click', function() {
    $(this).removeClass('active');
  });
  $('.modal-wrapper').on('click', function(event) {
    event.stopPropagation();
  });
  $('.fancybox').fancybox({
    padding:0,
  });
  function submitForm(form, e){
    var data = $(form).serialize();
    var text = $(form).find('button').attr('data-content');
    var page = $("input[name='htmlData']").val();
    var pagewoutslash = page.replace('/','');
    $.ajax({
          url: 'sendmessage.php',
          type: 'POST',
          data: data,
          beforeSend: function(){
            $(form).find('input, button').attr('disabled', '');
            $(form).find('button').attr('data-content', 'Отправляем...');
          }
        })
        .done(function(response) {
          $(form).find('input, button').removeAttr('disabled');
          $(form).find('input[name="phone"]').val('');
          dataLayer.push({
            'event' : 'VirtualPageview',
            'virtualPageURL' : page,
            'virtualPageTitle' : pagewoutslash
          });

          $(form).find('button').attr('data-content',text);
          $('[data-type="modal"]').removeClass('active');
          $('#response-modal').addClass('active');
        })
        .fail(function(response) {
          console.log(response);
        });
  }

  //3d models
  $('#imageKotel10').reel({
    image:       'img/kotl10.jpg',
    frames:      72,
    frame:       9,
    footage:     8,
    speed:       0.2
  });
  $('#imageKotel12').reel({
    image:       'img/kotl12.jpg',
    frames:      72,
    frame:       9,
    footage:     8,
    speed:       0.2
  });
  $('#imageKotel15').reel({
    image:       'img/kotl15.jpg',
    frames:      72,
    frame:       9,
    footage:     8,
    speed:       0.2
  });
  $('#imageKotel18').reel({
    image:       'img/kotl18.jpg',
    frames:      72,
    frame:       9,
    footage:     8,
    speed:       0.2
  });
  $('#imageKotel20').reel({
    image:       'img/kotl20.jpg',
    frames:      72,
    frame:       9,
    footage:     8,
    speed:       0.2
  });

  //scrollDown
  function scrollDown(){
    var elementClick=$('.scr-iii__wrapper').offset().top;
    $('html,body').animate({scrollTop: elementClick},800);
    return false;
  };
  //прокручивание по клику на стрелку
  $('.scr-i__scroll-to').on('click',scrollDown);
  //валидация на площадь дома
  //$('#AreaForm').validate({
  //  rules: {
  //    area: {
  //      required: true,
  //      area: true,
  //    }
  //  },
  //  messages: {
  //    area: {
  //      required: 'Введите площадь',
  //      area: 'Значение в рамках 10-250'
  //    }
  //  },
  //  submitHandler: function(form, e){
  //    e.preventDefault();
  //    var areaInput = form.querySelector('[name="area"]');
  //    var value = +areaInput.value;// + перевод строчного элемента цифровой
  //    scrollDown();
  //    if (value>=0 && value< 90) {
  //      $('.tab-nav-0').click();
  //    }
  //    if (value>90 && value<= 120) {
  //      $('.tab-nav-1').click();
  //    }
  //    if (value>120 && value<= 155) {
  //      $('.tab-nav-2').click();
  //    }
  //    if (value>155 && value<= 180) {
  //      $('.tab-nav-3').click();
  //    }
  //    if (value>180 && value<= 250) {
  //      $('.tab-nav-4').click();
  //    }
  //  }
  //});
  $('.scr-production--slider').slick({
    centerMode: true,
    centerPadding: '200px',
    slidesToShow: 1
  });
  $('.scr-reviews--slider').slick({
    slidesToShow: 1,
    adaptiveHeight: true
  });
  $('#tabs').tabs();
});
