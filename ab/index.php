<?php session_start();
$site_url = 'http://teplota.in/ab';
if (array_key_exists('utm_referrer', $_GET)) {
  if (strpos($site_url, $_GET['utm_referrer'])===false) {
    $_SESSION['referer'] = $_GET['utm_referrer'];
  }
}
if (array_key_exists('utm_source', $_GET)) {
  if (strpos($site_url, $_GET['utm_source'])===false) {
    $_SESSION['sourse'] = $_GET['utm_source'];
  }
  if (strpos($site_url, $_GET['utm_term'])===false) {
    $_SESSION['term'] = $_GET['utm_term'];
  }
  if (strpos($site_url, $_GET['utm_campaign'])===false) {
    $_SESSION['campaign'] = $_GET['utm_campaign'];
  }
}
?>
<!DOCTYPE html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">

<head>
  <meta charset="UTF-8">
  <title>Буржуй</title>
  <meta name="description" content="твердотопливные котлы от производителя">
  <meta name="keywords" content="">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="js/libs/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/owl.carousel.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/flipclock.css">
  <link rel="stylesheet" href="js/libs/fancybox/jquery.fancybox.css">
  <meta name="viewport" content="width=1200">
  <script src="js/libs/jquery.min.js"></script>
  <script src="js/libs/jquery-ui/jquery-ui.min.js"></script>
  <script src="js/libs/maskedinput.min.js"></script>
  <script src="js/libs/validate/jquery.validate.min.js"></script>
  <script src="js/libs/fancybox/jquery.fancybox.pack.js"></script>
  <script src="js/tabs.js"></script>
  <script src="js/libs/owl.carousel.js"></script>
  <script src="js/libs/jquery.reel.js"></script>
  <script src="js/libs/flipclock.js"></script>
  <script type="text/javascript">
    var clock;
    $(document).ready(function() {
      var currentDate = new Date();
      var pastDate  = new Date(2016, currentDate.getMonth(), currentDate.getDate()+1);
      var diff =  pastDate.getTime() / 1000 - currentDate.getTime() / 1000;
      clock = $('#timer10').FlipClock(diff, {
        clockFace: 'HourlyCounter',
        showSeconds: true,
        countdown: true,
        language: 'ru'
      });
      clock = $('#timer12').FlipClock(diff, {
        clockFace: 'HourlyCounter',
        showSeconds: true,
        countdown: true,
        language: 'ru'
      });
      clock = $('#timer15').FlipClock(diff, {
        clockFace: 'HourlyCounter',
        showSeconds: true,
        countdown: true,
        language: 'ru'
      });
      clock = $('#timer18').FlipClock(diff, {
        clockFace: 'HourlyCounter',
        showSeconds: true,
        countdown: true,
        language: 'ru'
      });
      clock = $('#timer20').FlipClock(diff, {
        clockFace: 'HourlyCounter',
        showSeconds: true,
        countdown: true,
        language: 'ru'
      });
    });
  </script>
</head>

<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MLRTGW"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MLRTGW');</script>
<!-- End Google Tag Manager -->
  <section class="wrapper scr-i__wrapper">
    <header class="inner header">
      <span class="header__phone">
        Работаем без выходных
        <a class="header__link" href="tel:+380667893083">(097) 272-81-82</a>
        <a class="header__link" href="tel:+380975896160">(050) 020-99-07</a>
      </span>
      <img class="header__logo" src="img/logo.png" alt="Буржуй">
      <div class="header__buttonbox" tabindex="1">
        <div class="header__buttonbox__button" data-input="Консультация" data-init="modal" data-modal="#callback-modal2">
          Консультация онлайн
        </div>
      </div>
    </header>
    <div class="inner scr-i">
      <span class="scr-i__title" style="font-size: 36px;">
        Перестаньте переплачивать за отопление в 3 раза
      </span>
      <span class="scr-i__subtitle">
        купив технологичный котел с уникальным теплообменником ПЛОЩАДЬЮ 1,7м2
      </span>
      <!--<div class="scr-i__sticker">-->
        <!--новинка-->
        <!--<svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="svg-triangle">-->
          <!--<polygon points="0 ,0 16,0 8.0 16"/>-->
        <!--</svg>-->
      <!--</div>-->
      <img class="scr-i__sale" src="img/action.jpg" alt="">
      <div class="scr-i__form-wrapper">
        <div class="scr-i__form-content">
          <span class="scr-i__form-title">
            Подберите котел за 5 минут
          </span>
          <span class="scr-i__form-subtitle">
            для Вашего дома
          </span>
          <div class="scr-i__form">
            <form class="formGo" action="sendmessage.php" method="POST">
              <div class="scr-i__input-wrapper">
                <input type="text" class="scr-i__input" placeholder="Введите площадь дома"   name="area" >
              </div>
              <div class="scr-i__input-wrapper">
                <input type="text"  class="scr-i__input" name="phone" placeholder="Введите Ваш телефон*">
              </div>
              <div class="scr-i__input-wrapper--wide">
                <button onsubmit="ga('send', 'pageview', '/header.html');" class="scr-i__button" type="submit" data-content="Подобрать котел"></button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <img class="scr-i__scroll-to" src="img/arrow-down.png" alt="">
    </div>
  </section>



  <section class="scr-iii__wrapper wrapper">
    <div class="scr-iii__inner inner">
      <span class="scr-iii__title">
        продукция
      </span>
      <div id="tabs" class="tabs">
        <img src="img/garanty.png" class="guarantee" alt="">
        <div class="tabs-nav">
          <ul>
            <li class="tabs-nav__item" data-content="Буржуй КП-10"></li>
            <li class="tabs-nav__item" data-content="Буржуй КП-12"></li>
            <li class="tabs-nav__item" data-content="Буржуй КП-15"></li>
            <li class="tabs-nav__item" data-content="Буржуй КП-18"></li>
            <li class="tabs-nav__item" data-content="Буржуй КП-20"></li>
          </ul>
        </div>
        <div class="tabs-content">
          <ul>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img id="imageKotel10" src="img/kotl101.jpg" width="500" height="375" />
                  </div>
                </div>
                <p class="power">Мощность 10 кВт</p>
                <a class="look_incision fancybox" href="img/incision.jpg">Посмотреть в разрезе</a>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="line">
                    <span class="declaration">Описание</span>
                    <a class="specification-button fancybox" href="#kp10">Технические характеристики</a>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь</span><span>100м2</span>
                  </div>
                  <div class="line">
                    <span>Глубина топки</span><span>50 см</span>
                  </div>
                  <div class="line">
                    <span> Площадь теплообменника</span><span>0,9 м2</span>
                  </div>
                  <div class="line">
                    <span>Вес котла</span><span>76 кг.</span>
                  </div>
                  <div class="line">
                    <span>Место под ТЭН</span><span>Есть</span>
                  </div>
                  <div class="plate">+варочная плита</div>
                </div>
                <div class="salebox">
                  <div class="sale">Акция</div>
                  <div class="sale_descr">
                    Успейте получитить скидку на котел<br> и бесплатную доставку!
                  </div>
                  <div class="counttimer">
                    До конца акции осталось:
                  </div>
                  <div class="doIt">Количество акционных<br> товаров ограничено</div>
                  <div id="timer10"></div>
                  <div class="button" data-content="Узнать подробности" data-input="Узнать подробности КП-10"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                </div>
                <p class="ask">
                  или задайте вопрос специалисту
                </p>
                <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                  <div class="tabs-content__input-wrapper">
                    <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите Ваш телефон*">
                  </div>
                  <input type="hidden" value="КП-10 задать вопрос" name="order">
                  <div class="tabs-content__input-wrapper">
                    <button onsubmit="ga('send', 'pageview', '/vopros.html');" type="submit" class="tabs-content__button" data-content="Задать вопрос"></button>
                  </div>
                </form>
              </div>
            </li>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img id="imageKotel12" src="img/kotl121.jpg" width="500" height="375" />
                  </div>
                </div>
                <p class="power">Мощность 12 кВт</p>
                <a class="look_incision fancybox" href="img/incision.jpg">Посмотреть в разрезе</a>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="line">
                    <span class="declaration">Описание</span>
                    <a class="specification-button fancybox" href="#kp12">Технические характеристики</a>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь</span><span>120м2</span>
                  </div>
                  <div class="line">
                    <span>Глубина топки</span><span>42 см</span>
                  </div>
                  <div class="line">
                    <span> Площадь теплообменника</span><span>1,1 м2</span>
                  </div>
                  <div class="line">
                    <span>Вес котла</span><span>87 кг.</span>
                  </div>
                  <div class="line">
                    <span>Место под ТЭН</span><span>Есть</span>
                  </div>
                  <div class="plate">+варочная плита</div>
                </div>
                <div class="salebox">
                  <div class="sale">Акция</div>
                  <div class="sale_descr">
                    Успейте получитить скидку на котел<br> и бесплатную доставку!
                  </div>
                  <div class="counttimer">
                    До конца акции осталось:
                  </div>
                  <div class="doIt">Количество акционных<br> товаров ограничено</div>
                  <div id="timer12"></div>
                  <div class="button" data-content="Узнать подробности" data-input="Узнать подробности КП-12"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                </div>
                <p class="ask">
                  или задайте вопрос специалисту
                </p>
                <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                  <div class="tabs-content__input-wrapper">
                    <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите Ваш телефон*">
                  </div>
                  <input type="hidden" value="КП-12 задать вопрос" name="order">
                  <div class="tabs-content__input-wrapper">
                    <button onsubmit="ga('send', 'pageview', '/vopros.html');" type="submit" class="tabs-content__button" data-content="Задать вопрос"></button>
                  </div>
                </form>
              </div>
            </li>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img id="imageKotel15" src="img/kotl151.jpg" width="500" height="375" />
                  </div>
                </div>
                <p class="power">Мощность 15 кВт</p>
                <a class="look_incision fancybox" href="img/incision.jpg">Посмотреть в разрезе</a>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="line">
                    <span class="declaration">Описание</span>
                    <a class="specification-button fancybox" href="#kp15">Технические характеристики</a>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь</span><span>150м2</span>
                  </div>
                  <div class="line">
                    <span>Глубина топки</span><span>42 см</span>
                  </div>
                  <div class="line">
                    <span> Площадь теплообменника</span><span>1,2 м2</span>
                  </div>
                  <div class="line">
                    <span>Вес котла</span><span>80 кг.</span>
                  </div>
                  <div class="line">
                    <span>Место под ТЭН</span><span>Есть</span>
                  </div>
                  <!--<div class="plate">+варочная плита</div>-->
                </div>
                <div class="salebox">
                  <div class="sale">Акция</div>
                  <div class="sale_descr">
                    Успейте получитить скидку на котел<br> и бесплатную доставку!
                  </div>
                  <div class="counttimer">
                    До конца акции осталось:
                  </div>
                  <div class="doIt">Количество акционных<br> товаров ограничено</div>
                  <div id="timer15"></div>
                  <div class="button" data-content="Узнать подробности" data-input="Узнать подробности КП-15"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                </div>
                <p class="ask">
                  или задайте вопрос специалисту
                </p>
                <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                  <div class="tabs-content__input-wrapper">
                    <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите Ваш телефон*">
                  </div>
                  <input type="hidden" value="КП-15 задать вопрос" name="order">
                  <div class="tabs-content__input-wrapper">
                    <button onsubmit="ga('send', 'pageview', '/vopros.html');" type="submit" class="tabs-content__button" data-content="Задать вопрос"></button>
                  </div>
                </form>
              </div>
            </li>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img id="imageKotel18" src="img/kotl181.jpg" width="500" height="375" />
                  </div>
                </div>
                <p class="power">Мощность 18 кВт</p>
                <a class="look_incision fancybox" href="img/incision.jpg">Посмотреть в разрезе</a>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="line">
                    <span class="declaration">Описание</span>
                    <a class="specification-button fancybox" href="#kp18">Технические характеристики</a>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь</span><span>180м2</span>
                  </div>
                  <div class="line">
                    <span>Глубина топки</span><span>63 см</span>
                  </div>
                  <div class="line">
                    <span> Площадь теплообменника</span><span>1,575 м2</span>
                  </div>
                  <div class="line">
                    <span>Вес котла</span><span>128 кг.</span>
                  </div>
                  <div class="line">
                    <span>Место под ТЭН</span><span>Есть</span>
                  </div>
                  <div class="plate">+варочная плита</div>
                </div>
                <div class="salebox">
                  <div class="sale">Акция</div>
                  <div class="sale_descr">
                    Успейте получитить скидку на котел<br> и бесплатную доставку!
                  </div>
                  <div class="counttimer">
                    До конца акции осталось:
                  </div>
                  <div class="doIt">Количество акционных<br> товаров ограничено</div>
                  <div id="timer18"></div>
                  <div class="button" data-content="Узнать подробности" data-input="Узнать подробности КП-18"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                </div>
                <p class="ask">
                  или задайте вопрос специалисту
                </p>
                <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                  <div class="tabs-content__input-wrapper">
                    <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите Ваш телефон*">
                  </div>
                  <input type="hidden" value="КП-18 задать вопрос" name="order">
                  <div class="tabs-content__input-wrapper">
                    <button onsubmit="ga('send', 'pageview', '/vopros.html');" type="submit" class="tabs-content__button" data-content="Задать вопрос"></button>
                  </div>
                </form>
              </div>
            </li>
            <li class="tabs-content__item tab">
              <div class="tabs-content__colum">
                <div class="box_player">
                  <div class="player3d">
                    <img id="imageKotel20" src="img/kotl201.jpg" width="500" height="375" />
                  </div>
                </div>
                <p class="power">Мощность 20 кВт</p>
                <a class="look_incision fancybox" href="img/incision.jpg">Посмотреть в разрезе</a>
              </div>
              <div class="tabs-content__colum--2">
                <div class="specifications">
                  <div class="line">
                    <span class="declaration">Описание</span>
                    <a class="specification-button fancybox" href="#kp20">Технические характеристики</a>
                  </div>
                  <div class="line">
                    <span>Отапливаемая площадь</span><span>200м2</span>
                  </div>
                  <div class="line">
                    <span>Глубина топки</span><span>63 см</span>
                  </div>
                  <div class="line">
                    <span> Площадь теплообменника</span><span>1,7 м2</span>
                  </div>
                  <div class="line">
                    <span>Вес котла</span><span>115 кг.</span>
                  </div>
                  <div class="line">
                    <span>Место под ТЭН</span><span>Есть</span>
                  </div>
                  <!--<div class="plate">+варочная плита</div>-->
                </div>
                <div class="salebox">
                  <div class="sale">Акция</div>
                  <div class="sale_descr">
                    Успейте получитить скидку на котел<br> и бесплатную доставку!
                  </div>
                  <div class="counttimer">
                    До конца акции осталось:
                  </div>
                  <div class="doIt">Количество акционных<br> товаров ограничено</div>
                  <div id="timer20"></div>
                  <div class="button" data-content="Узнать подробности" data-input="Узнать подробности КП-20"  data-init="modal" data-google="/productzakaz.html"  data-modal="#callback-modal"></div>
                </div>
                <p class="ask">
                  или задайте вопрос специалисту
                </p>
                <form class="tabs-content__form-wrapper formGo" action="sendmessage.php" method="POST">
                  <div class="tabs-content__input-wrapper">
                    <input type="text"  class="tabs-content__input" name="phone" placeholder="Введите Ваш телефон*">
                  </div>
                  <input type="hidden" value="КП-20 задать вопрос" name="order">
                  <div class="tabs-content__input-wrapper">
                    <button onsubmit="ga('send', 'pageview', '/vopros.html');" type="submit" class="tabs-content__button" data-content="Задать вопрос"></button>
                  </div>
                </form>
              </div>
            </li>
          </ul>
        </div>

      </div>
    </div>
  </section>

  <section class="scr-vi__wrapper wrapper">
    <div class="scr-vi__inner inner">
      <h2 class="scr-vi__fiches">
        Всего <span style="color: #00e4ff;font-size:35px;">1 клиент</span> обратился по <span style="color: #00e4ff;font-size:27px;">гарантии</span></br>
        из <span style="color: #00e4ff;font-size:38px;">4000</span> проданных котлов
      </h2>
      <h2 class="scr-vi__fiches">
        Дверца у наших котлов<br>
        открывается <span style="color: #00e4ff;font-size:28px;">во все стороны</span>
      </h2>
      <h2 class="scr-vi__fiches">
        Котел «Буржуй» является </br>
        <span style="color: #00e4ff;text-decoration: underline;font-size:33px;">сертифицированными</span><br> и соответствует всем условиям
      </h2>
      <h2 class="scr-vi__fiches">
        Мы добиваемся такого</br>
        качества так как <span style="color: #00e4ff; font-size: 28px;">проверяем</span> котлы</br>
        на каждом этапе производства <span style="color: #ffd800; font-size: 35px;">дважды</span>
      </h2>
      <div class="scr-vi__playbutton fancybox fancybox.iframe" href="https://www.youtube.com/embed/s0bH2JPrUq0?autoplay=true">
        <img src="img/play.png" alt="">
        <p>Посмотреть видео</p>
      </div>
    </div>
  </section>

  <section class="scr-ix__wrapper wrapper">
    <div class="scr-ix__inner inner">
      <div class="scr-ix__title">
        Почему 1250 человек по всей Украине
        выбрали котел "Буржуй" в 2015 году
      </div>
      <div class="scr-ix__col">
        <div class="scr-ix__col--fiches">ЭФФЕКТИВНЫЙ</div>
        <div class="scr-ix__col--descr">КПД котла более 80%</div>
        <div class="scr-ix__col--fiches">ДОСТУПНЫЙ</div>
        <div class="scr-ix__col--descr">В два раза дешевле<br>иностранных аналогов</div>
        <div class="scr-ix__col--fiches">НАДЕЖНЫЙ</div>
        <div class="scr-ix__col--descr">Толщина стали 3мм,<br>все узлы доступны<br>для обслуживания</div>
        <div class="scr-ix__col--fiches">ДОЛГОВЕЧНЫЙ</div>
        <div class="scr-ix__col--descr">Расчетный срок службы <br>не менее 10 лет</div>
        <div class="scr-ix__col--fiches">Варочная поверхность</div>
        <div class="scr-ix__col--descr">Изготовлена из чугуна,<br>можно использовать для готовки</div>
      </div>
      <div class="scr-ix__col2">
        <div class="scr-ix__col2--fiches">Большая<br> загрузочная камера</div>
        <div class="scr-ix__col2--fiches">Возможность <br>установки<br> автоматики</div>
        <div class="scr-ix__col2--fiches">Работает<br> на всех видах<br> твердого топлива</div>
        <div class="scr-ix__col2--fiches">Выход дымохода<br> в подарок</div>
      </div>
    </div>
  </section>


  <section class="scr-vii__wrapper wrapper">
    <div class="scr-vii__inner inner">
      <div class="scr-vii__title">
        Ответим на вопросы
      </div>
      <form class="scr-vii__form-wrapper formGo" action="sendmessage.php" method="POST">
        <div class="scr-vii__title-form">
          Оставьте Ваш телефон
        </div>
        <div class="scr-vii__input-wrapper">
          <input type="text"  class="scr-vii__input" name="phone" placeholder="Введите телефон">
        </div>
        <div class="scr-vii__input-wrapper">
          <button onsubmit="ga('send', 'pageview', '/konsult.html');"   type="submit" class="scr-vii__button" data-content="Получить консультацию"></button>
        </div>
        <div class="scr-vii__descr">
          специалист ответит на все
          интересующие Вас вопросы
        </div>
      </form>
    </div>
  </section>


  <div id="kp10" class="popup_descr">
    <p class="popup_descr_tittle">Технические характеристики твердотопливного котла <br> Буржуй<sup>ТМ</sup> КП10</p>
    <table>
      <tbody><tr><td>Номинальная мощность, кВт </td><td>10</td>
      </tr>
      <tr>
        <td>Отапливаемая площадь до м2 </td><td>100</td>
      </tr>
      <tr>
        <td>Объем теплоносителя (воды) в котле, литров, не менее</td><td>32</td>
      </tr>
      <tr>
        <td>КПД при работе в отопительном режиме, % не менее </td><td>80</td>
      </tr>
      <tr>
        <td>Эффективное рабочее давление, МПа</td><td>0,1 МПа</td>
      </tr>
      <tr>
        <td>Максимальное рабочее давление, МПа </td><td>0,2 МПа</td>
      </tr>
      <tr>
        <td>Эффективная температура теплоносителя, °С </td><td>min 60 °C - max 90 °C</td>
      </tr>
      <tr>
        <td>Расход каменного угля,  кг/час  </td><td>1</td>
      </tr>
      <tr>
        <td>Наружные размеры дымохода, мм </td><td>Д140</td>
      </tr>
      <tr>
        <td>Площадь сечения дымохода, м2 </td><td>0,0091</td>
      </tr>
      <tr>
        <td>Минимальная высота дымохода,от уровня колосников до верхнего среза трубы, м</td><td>6</td>
      </tr>
      <tr>
        <td>Диаметр входного и выходного патрубков, дюйма </td><td>G1 1/4</td>
      </tr>
      <tr>
        <td>Габаритные размеры, мм:</td><td></td>
      </tr>
      <tr>
        <td>Высота </td><td>600</td>
      </tr>
      <tr>
        <td>Ширина </td><td>372</td>
      </tr>
      <tr>
        <td>Глубина</td><td>631</td>
      </tr>
      <tr>
        <td>Масса кг, не более </td><td>76</td></tr><tr>
      </tr>
      </tbody></table>
  </div>


  <div id="kp12" class="popup_descr">
    <p class="popup_descr_tittle">Технические характеристики твердотопливного котла Буржуй<sup>ТМ</sup> КП12</p>
    <table>
      <tbody><tr><td>Номинальная мощность, кВт </td><td>12</td>
      </tr>
      <tr>
        <td>Отапливаемая площадь до м2 </td><td>120</td>
      </tr>
      <tr>
        <td>Объем теплоносителя (воды) в котле, литров, не менее</td><td>48</td>
      </tr>
      <tr>
        <td>КПД при работе в отопительном режиме, % не менее </td><td>80</td>
      </tr>
      <tr>
        <td>Эффективное рабочее давление, МПа</td><td>0,1 МПа</td>
      </tr>
      <tr>
        <td>Максимальное рабочее давление, МПа </td><td>0,2 МПа</td>
      </tr>
      <tr>
        <td>Эффективная температура теплоносителя, °С </td><td>min 60 °C - max 90 °C</td>
      </tr>
      <tr>
        <td>Расход каменного угля,  кг/час  </td><td>1,2</td>
      </tr>
      <tr>
        <td>Наружные размеры дымохода, мм </td><td>80x311</td>
      </tr>
      <tr>
        <td>Площадь сечения дымохода, м2 </td><td>0,0188</td>
      </tr>
      <tr>
        <td>Минимальная высота дымохода,от уровня колосников до верхнего среза трубы, м</td><td>6</td>
      </tr>
      <tr>
        <td>Диаметр входного и выходного патрубков, дюйма </td><td>G2"</td>
      </tr>
      <tr>
        <td>Габаритные размеры, мм: </td><td></td>
      </tr>
      <tr>
        <td>Высота </td><td>718</td>
      </tr>
      <tr>
        <td>Ширина </td><td>459</td>
      </tr>
      <tr>
        <td>Глубина</td><td>560</td>
      </tr>
      <tr>
        <td>Масса кг, не более </td><td>87</td></tr><tr>
      </tr>
      </tbody></table>
  </div>

  <div id="kp15" class="popup_descr">
    <p class="popup_descr_tittle">Технические характеристики твердотопливного котла Буржуй<sup>ТМ</sup> КП15(скоро в продаже!)</p>
    <table>
      <tbody><tr><td>Номинальная мощность, кВт </td><td>15</td>
      </tr>
      <tr>
        <td>Отапливаемая площадь до м2 </td><td>150</td>
      </tr>
      <tr>
        <td>Объем теплоносителя (воды) в котле, литров, не менее</td><td>56</td>
      </tr>
      <tr>
        <td>КПД при работе в отопительном режиме, % не менее </td><td>80</td>
      </tr>
      <tr>
        <td>Эффективное рабочее давление, МПа</td><td>0,1 МПа</td>
      </tr>
      <tr>
        <td>Максимальное рабочее давление, МПа </td><td>0,2 МПа</td>
      </tr>
      <tr>
        <td>Эффективная температура теплоносителя, °С </td><td>min 60 °C - max 90 °C</td>
      </tr>
      <tr>
        <td>Расход каменного угля,  кг/час  </td><td>1,3</td>
      </tr>
      <tr>
        <td>Наружные размеры дымохода, мм </td><td>80x311</td>
      </tr>
      <tr>
        <td>Площадь сечения дымохода, м2 </td><td>0,0188</td>
      </tr>
      <tr>
        <td>Минимальная высота дымохода,от уровня колосников до верхнего среза трубы, м</td><td>6</td>
      </tr>
      <tr>
        <td>Диаметр входного и выходного патрубков, дюйма </td><td>G2"</td>
      </tr>
      <tr>
        <td>Габаритные размеры, мм: </td><td></td>
      </tr>
      <tr>
        <td>Высота </td><td>718</td>
      </tr>
      <tr>
        <td>Ширина </td><td>459</td>
      </tr>
      <tr>
        <td>Глубина</td><td>560</td>
      </tr>
      <tr>
        <td>Масса кг, не более </td><td>80</td></tr><tr>
      </tr>
      </tbody></table>
  </div>


  <div id="kp18" class="popup_descr">
    <p class="popup_descr_tittle">Технические характеристики твердотопливного котла Буржуй<sup>ТМ</sup> КП18</p>
    <table>
      <tbody><tr><td>Номинальная мощность, кВт </td><td>18</td>
      </tr>
      <tr>
        <td>Отапливаемая площадь до м2 </td><td>180</td>
      </tr>
      <tr>
        <td>Объем теплоносителя (воды) в котле, литров, не менее</td><td>88</td>
      </tr>
      <tr>
        <td>КПД при работе в отопительном режиме, % не менее </td><td>80</td>
      </tr>
      <tr>
        <td>Эффективное рабочее давление, МПа</td><td>0,1 МПа</td>
      </tr>
      <tr>
        <td>Максимальное рабочее давление, МПа </td><td>0,2 МПа</td>
      </tr>
      <tr>
        <td>Эффективная температура теплоносителя, °С </td><td>min 60 °C - max 90 °C</td>
      </tr>
      <tr>
        <td>Расход каменного угля,  кг/час  </td><td>1,6</td>
      </tr>
      <tr>
        <td>Наружные размеры дымохода, мм </td><td>80*311</td>
      </tr>
      <tr>
        <td>Площадь сечения дымохода, м2 </td><td>0,0188</td>
      </tr>
      <tr>
        <td>Минимальная высота дымохода,от уровня колосников до верхнего среза трубы, м</td><td>6</td>
      </tr>
      <tr>
        <td>Диаметр входного и выходного патрубков, дюйма </td><td>G2"</td>
      </tr>
      <tr>
        <td>Габаритные размеры, мм: </td><td></td>
      </tr>
      <tr>
        <td>Высота </td><td>749</td>
      </tr>
      <tr>
        <td>Ширина </td><td>459</td>
      </tr>
      <tr>
        <td>Глубина</td><td>814</td>
      </tr>
      <tr>
        <td>Масса кг, не более </td><td>128</td></tr><tr>
      </tr>
      </tbody></table>
  </div>

  <div id="kp20" class="popup_descr">
    <p class="popup_descr_tittle">Технические характеристики твердотопливного котла Буржуй<sup>ТМ</sup> КП20</p>
    <table>
      <tbody><tr><td>Номинальная мощность, кВт </td><td>20</td>
      </tr>
      <tr>
        <td>Отапливаемая площадь до м2 </td><td>200</td>
      </tr>
      <tr>
        <td>Объем теплоносителя (воды) в котле, литров, не менее</td><td>103</td>
      </tr>
      <tr>
        <td>КПД при работе в отопительном режиме, % не менее </td><td>80</td>
      </tr>
      <tr>
        <td>Эффективное рабочее давление, МПа</td><td>0,1 МПа</td>
      </tr>
      <tr>
        <td>Максимальное рабочее давление, МПа </td><td>0,2 МПа</td>
      </tr>
      <tr>
        <td>Эффективная температура теплоносителя, °С </td><td>min 60 °C - max 90 °C</td>
      </tr>
      <tr>
        <td>Расход каменного угля,  кг/час  </td><td>1,8</td>
      </tr>
      <tr>
        <td>Наружные размеры дымохода, мм </td><td>80x311</td>
      </tr>
      <tr>
        <td>Площадь сечения дымохода, м2 </td><td>0,0188</td>
      </tr>
      <tr>
        <td>Минимальная высота дымохода,от уровня колосников до верхнего среза трубы, м</td><td>6</td>
      </tr>
      <tr>
        <td>Диаметр входного и выходного патрубков, дюйма </td><td>G2"</td>
      </tr>
      <tr>
        <td>Габаритные размеры, мм: </td><td></td>
      </tr>
      <tr>
        <td>Высота </td><td>749</td>
      </tr>
      <tr>
        <td>Ширина </td><td>459</td>
      </tr>
      <tr>
        <td>Глубина</td><td>814</td>
      </tr>
      <tr>
        <td>Масса кг, не более </td><td>115</td></tr><tr>
      </tr>
      </tbody></table>
  </div>



  <div class="modal callback-modal" id="callback-modal" data-type="modal">
    <div class="modal__wrapper modal-wrapper">
      <span class="modal__close modal-close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Оставьте заявку на котел
      </span>
      <span class="modal__subtitle">
        и менеджер свяжется с вами в течении 5 минут
      </span>
        <form class="formGo" action="sendmessage.php" method="POST" >
          <div class="modal__input-wrapper">
            <label class="modal__label" for="callback-modal-text-1">Введите номер телефона</label>
            <input type="text" id="callback-modal-text-1" class="modal__input" name="phone" placeholder="+38 (0__) ___-__-__">
          </div>
          <div class="modal__input-wrapper">
            <input type="text"  class="modal__input" placeholder="Введите Ваше имя" name="name">
          </div>
          <input type="hidden" value="" name="order" id="order">
          <div class="modal__input-wrapper">
            <button onsubmit="ga('send', 'pageview', '/konsult.html');"  type="submit" class="modal__button" data-content="Заказать"></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal callback-modal" id="callback-modal2" data-type="modal">
    <div class="modal__wrapper modal-wrapper">
      <span class="modal__close modal-close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Оставьте заявку
      </span>
      <span class="modal__subtitle">
        и менеджер свяжется с вами в течении 5 минут
      </span>
        <form class="formGo" action="sendmessage.php" method="POST" >
          <div class="modal__input-wrapper">
            <label class="modal__label" for="callback-modal-text-2">Введите номер телефона</label>
            <input type="text" id="callback-modal-text-2" class="modal__input" name="phone" placeholder="+38 (0__) ___-__-__">
          </div>
          <div class="modal__input-wrapper">
            <input type="text" class="modal__input" placeholder="Введите Ваше имя" name="name">
          </div>
          <div class="modal__input-wrapper">
            <button onsubmit="ga('send', 'pageview', '/callback.html');" type="submit" class="modal__button" data-content="Оставить заявку"></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal response-modal" data-type="modal" id="response-modal">
    <div class="modal__wrapper">
      <span class="modal__close">&times;</span>
      <div class="modal__inner">
      <span class="modal__title">
        Сообщение отправлено!<br>
        Мы свяжемся с Вами в ближайшее время.
      </span>
      </div>
    </div>
  </div>

  <script src="js/common.js"></script>
  <!-- Yandex.Metrika counter -->
<!--  <script type="text/javascript">-->
<!--    (function (d, w, c) {-->
<!--      (w[c] = w[c] || []).push(function() {-->
<!--        try {-->
<!--          w.yaCounter33290648 = new Ya.Metrika({id:33290648,-->
<!--            webvisor:true,-->
<!--            clickmap:true});-->
<!--        } catch(e) { }-->
<!--      });-->
<!---->
<!--      var n = d.getElementsByTagName("script")[0],-->
<!--              s = d.createElement("script"),-->
<!--              f = function () { n.parentNode.insertBefore(s, n); };-->
<!--      s.type = "text/javascript";-->
<!--      s.async = true;-->
<!--      s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";-->
<!---->
<!--      if (w.opera == "[object Opera]") {-->
<!--        d.addEventListener("DOMContentLoaded", f, false);-->
<!--      } else { f(); }-->
<!--    })(document, window, "yandex_metrika_callbacks");-->
<!--  </script>-->
<!--  <noscript><div><img src="//mc.yandex.ru/watch/33290648" style="position:absolute; left:-9999px;" alt="" /></div></noscript>-->
  <!-- /Yandex.Metrika counter -->
<!--  <script>-->
<!--    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){-->
<!--              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),-->
<!--            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)-->
<!--    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');-->
<!---->
<!--    ga('create', 'UA-74413080-1', 'auto');-->
<!--    ga('send', 'pageview');-->
<!---->
<!--  </script>-->

  
  <!— BEGIN JIVOSITE CODE {literal} —>
  <script type='text/javascript'>
    (function(){ var widget_id = 'K66kAqWI7Q';
      var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
  <!— {/literal} END JIVOSITE CODE —>


<!--  <noindex>-->
<!--    <link rel="stylesheet" href="//cabinet.salesupwidget.com/widget/tracker.css">-->
<!--    <script type="text/javascript" src="//cabinet.salesupwidget.com/php/1.js" charset="UTF-8" async></script >-->
<!--    <script type="text/javascript">var uid_code="1019";</script>-->
<!--  </noindex>-->
</body>

</html>
